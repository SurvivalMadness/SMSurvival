package main.listeners;

import main.java.SMCore;
import main.java.player.PlayerManager;
import main.java.player.SMPlayer;
import main.java.rank.Rank;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Class created by Shiv M.
 */
public class PlayerQuit implements Listener {

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent e) {
        SMPlayer splayer = PlayerManager.getInstance().getPlayer(e.getPlayer());
        if (splayer.getRank() == Rank.DEFAULT) {
            e.setQuitMessage(SMCore.color("&7" + splayer.getCurrentName() + " &r&fhas joined &e&lSurvival&r&f."));
            return;
        }
        e.setQuitMessage(SMCore.color(splayer.getRank().getColor() + " " + splayer.getCurrentName() + " &r&fhas joined &e&lSurvival&r&f."));
    }
}
