package main.listeners;

import main.SMSurvival;
import main.java.SMCore;
import main.java.player.PlayerManager;
import main.java.player.SMPlayer;
import main.java.rank.Rank;
import main.java.rank.RankManager;
import main.java.utils.ItemUtil;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Created by Shiv - Any code in the following class is not to be used
 * without consent, is direct code owned privately by
 * Shiv, a.k.a Tormatic.
 */
public class PlayerJoin implements Listener {

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        final Player p = e.getPlayer();
        SMPlayer splayer = PlayerManager.getInstance().getPlayer(e.getPlayer());
        String uuid = p.getUniqueId().toString();
        if (RankManager.getInstance().getRank(p) == null) {
            new BukkitRunnable() {
                public void run() {
                    RankManager.getInstance().setRank(p, Rank.DEFAULT);
                }
            }.runTaskLater(SMCore.getPlugin(), 5L);
        }
        if (!(SMSurvival.getInstance().getConfig().contains("players." + uuid))) {
            SMSurvival.getInstance().getConfig().set("players." + uuid + ".dollars", 125);
            SMSurvival.getInstance().getConfig().set("players." + uuid + ".blocks_broken", 0);
            SMSurvival.getInstance().getConfig().set("players." + uuid + ".blocks_placed", 0);
            SMSurvival.getInstance().saveConfig();
        }
        p.sendMessage(SMCore.color(SMCore.prefix + "&eWelcome to &aSurvival &eon &6Survival Madness&e! To get started, do '&f/survival help&e'"));
        if (splayer.getRank() == Rank.DEFAULT) {
            e.setJoinMessage(SMCore.color("&7" + splayer.getCurrentName() + " &r&fhas joined &e&lSurvival&r&f."));
            return;
        }
        e.setJoinMessage(SMCore.color(splayer.getRank().getColor() + splayer.getRankPrefix() + " " + ChatColor.RESET + splayer.getRank().getColor() + splayer.getCurrentName() + " &r&fhas joined &e&lSurvival&f."));
        ItemStack help = ItemUtil.createItem(Material.WRITTEN_BOOK, SMCore.color("&e&lHELP BOOK! &7(Right Click)"));
        p.getInventory().setItem(4, help);
        p.getInventory().setHeldItemSlot(4);
    }

}
