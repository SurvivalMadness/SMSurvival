package main.commands;

import main.SMSurvival;
import main.java.SMCore;
import main.java.player.PlayerManager;
import main.java.player.SMPlayer;
import main.java.rank.Rank;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Class created by Shiv M.
 */
public class AddDollars implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("adddollars")) {
            Player p = (Player) sender;
            SMPlayer pl = PlayerManager.getInstance().getPlayer(p);
            if (!(pl.getRank() == Rank.ADMIN || pl.getRank() == Rank.OWNER || pl.getRank() == Rank.DEV)) {
                p.sendMessage(SMCore.noperm);
                return true;
            }
            SMSurvival.getInstance().saveConfig();
        }
        return true;
    }
}
