package main.shop;

import main.SMSurvival;
import main.java.SMCore;
import main.java.player.PlayerManager;
import main.java.player.SMPlayer;
import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_8_R3.ItemSaddle;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Class created by Shiv M.
 */
public class PlayerInteract implements Listener {

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getClickedBlock().getState() instanceof Sign) {
                Player p = e.getPlayer();
                SMPlayer pl = PlayerManager.getInstance().getPlayer(e.getPlayer());
                Sign sign = (Sign) e.getClickedBlock().getState();
                if (sign.getLine(0).equalsIgnoreCase(SMCore.color("&a[Buy]"))) {
                    int currentmoney = SMSurvival.getInstance().getConfig().getInt("players." + p.getUniqueId().toString() + ".dollars");
                    String newLine = sign.getLine(2).replace("Cost: ", "");
                    int neededmoney = Integer.parseInt(ChatColor.stripColor(newLine));
                    if (currentmoney < neededmoney) {
                        p.sendMessage(SMCore.color("&cYou don't have the proper funds to purchase this item."));
                        e.setCancelled(true);
                        return;
                    }
                    String item = ChatColor.stripColor(sign.getLine(1).replace("Item: ", ""));
                    ItemStack shopitem = new ItemStack(Material.getMaterial(Integer.parseInt(item)));
                    if (Material.getMaterial(Integer.parseInt(item)) == null) {
                        p.sendMessage(SMCore.color("&cThat's not a real item, please ensure the ID is correct. Contact a Developer for further assistance."));
                        sign.getBlock().breakNaturally();
                        return;
                    }
                    shopitem.setAmount(Integer.parseInt(ChatColor.stripColor(sign.getLine(3).replace("Amount: ", ""))));
                    p.getInventory().addItem(shopitem);
                    p.sendMessage(SMCore.color("&aYou have successfully purchased an item &f-&6 " + ChatColor.stripColor(sign.getLine(2).replace("Cost: ", "")) + " &adollars deducted."));
                    System.out.println("[SELL] " + p.getName() + " purchased an item (" + sign.getLine(1) + ") from the shop successfully. Dollars deducted: " + sign.getLine(2) + ".");
                    SMSurvival.getInstance().getConfig().set("players." + p.getUniqueId().toString() + ".dollars", currentmoney - neededmoney);
                    SMSurvival.getInstance().saveConfig();
                }
            }
        }
    }
}
