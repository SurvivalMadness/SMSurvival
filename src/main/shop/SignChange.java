package main.shop;

import main.java.SMCore;
import main.java.player.PlayerManager;
import main.java.player.SMPlayer;
import main.java.rank.Rank;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

/**
 * Class created by Shiv M.
 */
public class SignChange implements Listener {

    @EventHandler
    public void onSignChange(SignChangeEvent e) {
        if (e.getLine(0).equalsIgnoreCase("[Sell]")) {
            Player p = e.getPlayer();
            SMPlayer pl = PlayerManager.getInstance().getPlayer(e.getPlayer());
            if (!(pl.getRank() == Rank.ADMIN || pl.getRank() == Rank.OWNER || pl.getRank() == Rank.DEV)) {
                p.sendMessage(SMCore.color("&cYou cannot create Sell signs."));
                e.getBlock().breakNaturally();
                return;
            }
            e.setLine(0, SMCore.color("&a[Buy]"));
            e.setLine(1, SMCore.color("&3&lItem: &f" + e.getLine(1)));
            e.setLine(2, SMCore.color("&3&lCost: &f" + e.getLine(2)));
            e.setLine(3, SMCore.color("&3&lAmount: &f" + e.getLine(3)));
        }
    }
}
