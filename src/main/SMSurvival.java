package main;

import main.commands.AddDollars;
import main.java.player.SMPlayer;
import main.listeners.PlayerJoin;
import main.listeners.PlayerQuit;
import main.shop.PlayerInteract;
import main.shop.SignChange;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Shiv - Any code in the following class is not to be used
 * without consent, is direct code owned privately by
 * Shiv, a.k.a Tormatic.
 */
public class SMSurvival extends JavaPlugin {

    private static SMSurvival instance;

    public static SMSurvival getInstance() {
        return instance;
    }

    public void onEnable() {
        instance = this;
        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
            getConfig().options().copyDefaults(true);
            saveDefaultConfig();
            System.out.println("MICK DER!");
        }
        PluginManager pm = Bukkit.getServer().getPluginManager();
        pm.registerEvents(new PlayerJoin(), this);
        pm.registerEvents(new PlayerQuit(), this);
        pm.registerEvents(new PlayerInteract(), this);
        pm.registerEvents(new SignChange(), this);
        getCommand("adddollars").setExecutor(new AddDollars());
        saveConfig();
    }

}
